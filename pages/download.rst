.. title: Download
.. slug: download
.. date: 2016-12-03 10:21:31 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
.. nocomments: True

.. note::

  **The openATTIC project has entered maintenance mode. New feature development
  has been discontinued in favor of enhancing the upstream Ceph Dashboard.**

  See :doc:`this blog post <the-openattic-project-enters-maintenance-mode>` for
  details.

Source Code
-----------

The openATTIC source code is managed in a `git repository on BitBucket
<https://bitbucket.org/openattic/openattic/>`_.

To create a local clone of the repository, simply run the following command::

    $ git clone https://bitbucket.org/openattic/openattic.git

If you plan to contribute to the code, you should `fork the project on BitBucket
<http://docs.openattic.org/en/latest/developer_docs/git.html>`_ and work on your
changes there.

Each official release of openATTIC as well as nightly snapshots are also
available as source tar archives, which can be downloaded from `here
<http://download.openattic.org/sources/>`_

Installing openATTIC from packages
----------------------------------

openATTIC can be installed on Linux only.

There is no individual packaged download of the software - we provide dedicated
package repositories to facilitate the easy installation and automatic updating
using the Linux distribution's built-in package management.

Packages for SUSE Linux (openSUSE Leap) can be obtained from
the `openSUSE Build Service <http://software.opensuse.org/>`_.

See the `openATTIC Installation and Getting Started Guide
<http://docs.openattic.org/en/latest/install_guides/>`_ for instructions on how to
subscribe to these repositories in order to install these packages using your
distribution's package management utilities.