.. title: Getting Involved
.. slug: get-involved
.. date: 2016-11-19 10:21:48 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
.. nocomments: True

.. note::

  **The openATTIC project has entered maintenance mode. New feature development
  has been discontinued in favor of enhancing the upstream Ceph Dashboard.**

  See :doc:`this blog post <the-openattic-project-enters-maintenance-mode>` for
  details.

Need help?
----------

There are multiple resources available for you to get help, in case you have
questions or run into problems when using openATTIC.

As a first step, please make sure to consult the `official documentation
<http://docs.openattic.org/>`_ for help on getting openATTIC installed and
configured. Our `public Wiki
<https://wiki.openattic.org/>`_ might also provide additional insight.

Getting in touch
----------------

There are many ways for you to get in touch with us. We prefer having
conversations on our public channels like the mailing list or IRC, as this makes
it easier for other users to chime in and to contribute to the discussions. It
also allows new users to read up on previous topics and learn about know issues
and workarounds.

Join our Google Group
---------------------

We have set up a discussion group `openATTIC Users
<https://groups.google.com/forum/#!forum/openattic-users>`_ on Google Groups,
and we'd like to invite you to join the conversations. If you have a question or
problem, just ask! Google Groups offers a forum-like web frontend and can also
be used as regular mailing list.

You can subscribe to it via the web page, or via email, by sending an empty
message to openattic-users+subscribe@googlegroups.com, using the email address
that you want to use to exchange messages with this group (no Google account
required).

Before asking a question, please consider searching the group's archive and
consult our `public issue tracker <https://tracker.openattic.org>`_ - maybe the
issue you are observing has already been reported.

Also see `How To Ask Questions The Smart Way
<http://catb.org/~esr/faqs/smart-questions.html>`_ by Eric S. Raymond and Rick
Moen for some more guidance on how to yield the best results when posting a
request for help.

Chat with us on IRC
-------------------

There is a public `#openattic IRC channel
<https://webchat.freenode.net/?channels=openattic>`_ on `irc.freenode.net
<irc://irc.freenode.net>`_. Most of the developers and some openATTIC users can
be found there and can be asked for help.

openATTIC on social networks
----------------------------

* Twitter: `@openattic <https://twitter.com/openattic>`_
* Google+: `+openATTIC <https://plus.google.com/+Openattic/>`_

Send us Feedback
----------------

Found a bug? Missing a feature? Please let us know!

If you want to help, you don’t have to be a developer. Reporting bugs or
inconsistencies also helps!

We have set up a `issue tracker <http://tracker.openattic.org/>`_ (based on
`Atlassian Jira <https://www.atlassian.com/software/jira/>`_). We appreciate any
kind of feedback like comments, enhancement requests, bug reports, or code
contributions!

Before submitting a bug report or feature request, please spend some time on
make sure it’s has not been reported already. If you're unsure, feel free to get
in touch with us via IRC or the Google group first, so we can discuss your
request beforehand.

Contribute Code or Patches
--------------------------

You have coding experience in `Python <https://www.python.org/>`_ (`Django
<https://www.djangoproject.com/>`_ and the `Django REST Framework
<http://www.django-rest-framework.org/>`_) or JavaScript (`AngularJS
<https://angularjs.org/>`_, `Bootstrap <http://getbootstrap.com/>`_) and would
like to contribute to openATTIC? Or you've found a bug and want to propose a
fix?

If you're looking for some easy task to get started, we have created a list of
relatively easy tasks that would be suitable for new developers to become
familar with the code base and processes involved. We've collected them on a
dedicated Wiki page titled `Low hanging fruit tasks
<https://wiki.openattic.org/display/OP/Low+hanging+fruit+tasks>`_. If you're
looking for something to hone your skills and expertise, these might be a good
start.

Please join our IRC channel or Google Group — it will help us to get to get to
know you better and how we can collaborate. Coordinating our work will allow us
to achieve the best results.

The openATTIC source code is available in the form of a `git repository
<https://bitbucket.org/openattic/openattic>`_ on BitBucket.

First off, start by `setting up a development system
<http://docs.openattic.org/en/latest/developer_docs/setup_howto.html#developer-setup-howto>`_.
Then code away, implementing whatever changes you want to make. See
`contributing code to openATTIC
<http://docs.openattic.org/en/latest/developer_docs/contribute.html#developer-contribute>`_
for details on how to submit your changes to the upstream developers. Follow the
`openATTIC Contributing Guidelines
<http://docs.openattic.org/en/latest/developer_docs/contributing_guidelines.html#developer-contributing-guidelines>`_
to make sure your patches will be accepted.

To improve tracking of who did what, we require you to `sign your patch contribution
<http://docs.openattic.org/en/latest/developer_docs/contributing_guidelines.html#signing-your-patch-contribution>`_.
The sign-off is a simple line at the end of the explanation for the patch, which
certifies that you wrote it or otherwise have the right to pass it on as an
open-source patch.
