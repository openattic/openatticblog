.. title: Blog comments and a shiny new Jira instance
.. slug: blog-comments-and-a-shiny-new-jira-instance
.. date: 2015-10-09 19:34:57 UTC+02:00
.. tags: news, collaboration, opensource, announcement, jira, blog
.. category: 
.. link: 
.. description: Announcing the blog comment system and public Jira instance
.. type: text
.. author: Lenz Grimmer

Page rendering speeds and security of a static web site or blog like ours can
not be beaten by any CMS or blog software that is developed in some kind of
scripting language using a database backend.

However, one downside is the lack of a feedback channel like a comment system.
As we're very eager to receive your feedback, we've now added a comment system
to this blog, powered by `Disqus <https://disqus.com/>`_. You can either leave
comments below any post, or visit the `openATTIC channel on Discqus
<https://disqus.com/home/channel/openattic/>`_ to join the conversation.

Another highlight worth mentioning: as already mentioned in a :doc:`previous
blog post <opening-up>`, we've applied for an Open Source License for
Atlassian Jira. They were kind enough to approve our request very quickly, so
we now have a `dedicated Jira instance <http://tracker.openattic.org/>`_ for
tracking the openATTIC development. We intend to enable self-registration, so
that you will be able to create new issues or comment on existing ones once
you've created an account and have logged in. Once this is in place, we'll
disable the bug tracker on BitBucket, to avoid confusion. Any open issues will
be migrated to Jira first, of course. We're very grateful for this generous
offer from Atlassian. Thank you!

Any many thanks to `Kai Wagner <link://author/kai-wagner>`_ for setting up
both the comment system and Jira in record time!
