.. title: Ceph Dashboard v2 update
.. slug: ceph-dashboard-v2-update
.. date: 2018-04-09 12:30:22 UTC+02:00
.. tags: ceph, community, development, mgr, dashboard
.. category: 
.. link: 
.. description: Summarizing the last month since the Dashboard was merged
.. type: text
.. author: Lenz Grimmer

It's been a little over a month now since we reached :doc:`Milestone
1<the-initial-ceph-dashboard-v2-pull-request-has-been-merged>` (feature parity
with Dashboard v1), which was `merged
<https://github.com/ceph/ceph/pull/20103>`_ into the Ceph ``master`` branch on
2018-03-06.

After the initial merge, we had to resolve a few build and packaging related
issues, to streamline the ongoing development, testing and packaging of the new
dashboard as part of the main Ceph project.

With these teething problems out of the way, the team has started working on
several topics in parallel. A lot of these are "groundwork/foundation" kind of
tasks, e.g. adding UI components and backend functionality that pave the way to
enable the additional user-visible management features.

In the meanwhile, we have submitted over 80 additional `pull requests
<https://github.com/ceph/ceph/pulls?q=is%3Aopen+is%3Apr+label%3Adashboard>`_, of
which more than 60 have been merged already.

In this post, I'd like to summarize some of the highlights and notable
improvements we're currently working on or that have been added to the code base
already. This is by no means a complete list - it's more a subjective selection
of changes that caught my attention.

It's also noteworthy that we've already received a number of pull requests from
Ceph community members outside of the original openATTIC team that started this
project - we're very grateful for the support and look forward to future
contributions!

.. TEASER_END

New backend improvements
========================

Let's start with highlighting a few backend improvements that have already been
merged:

* `PR#20870 <https://github.com/ceph/ceph/pull/20870>`_ added **asynchronous
  tasks management**, to support the execution of long-running tasks by
  dashboard backend controllers (e.g. RBD or Pool creation). The tasks are
  executed asynchronously and can be queried for their respective execution
  status. (The UI part of this is in review/test - see below)

* `PR#20873 <https://github.com/ceph/ceph/pull/20873>`_ added a **browseable
  REST API** that provides a simple HTML form to POST data to a RESTController's
  ``create()`` method via a web browser. This is primarily a tool for
  developers, but it's something that we found quite usable in openATTIC (where
  the Django REST framework provided a `similar feature
  <http://www.django-rest-framework.org/topics/browsable-api/>`_). 

  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-browsable-api.png

* `PR#20865 <https://github.com/ceph/ceph/pull/20865>`_ added support for
  **creating Ceph Pools via the REST API** - this is a prerequisite for the
  advanced pool management features of the dashboard.

* `PR#20751 <https://github.com/ceph/ceph/pull/20751>`_ added support for
  **creating RBDs via the REST API**, laying the foundation for the additional
  RBD management capabilities in the UI.

* `PR#21239 <https://github.com/ceph/ceph/pull/21239>`_ changed the type of 
  request dispatcher used in the dashboard backend from the simple/default
  CherryPy dispatcher to the **routes based dispatcher**. This change was
  triggered by the necessity of having more control on the URL patterns for the
  controllers, in particular for REST controllers.

New UI improvements
===================

We also added many UI components and widgets required for adding new mangement
features:

* `PR#21128 <https://github.com/ceph/ceph/pull/21128>`_: A **usage bar
  component** that provides a visual representation of the utilization of a
  given object. It's already enabled on the cluster health page, the CephFS pool
  list and the list of OSDs.

  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-usage-bars-pools-cephfs.png
  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-usage-bars-pools.png
  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-usage-bars-osds.png

* `PR#21087 <https://github.com/ceph/ceph/pull/21087>`_: A **custom date pipe**
  that can be used to format dates on table columns, e.g. when displaying
  creation dates.

  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-custom-date.png

* `PR#20972 <https://github.com/ceph/ceph/pull/20972>`_: A directive that can be
  used to provide a **"size" input** to the user, e.g. when defining the size of
  an RBD to be created.

  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-dimless-binary.png

* `PR#20971 <https://github.com/ceph/ceph/pull/20971>`_: A component that can be
  used to **provide additional information** to the user, e.g. to give some contextual help.

  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-help-component.png

* `PR#20757 <https://github.com/ceph/ceph/pull/20757>`_ fixed a small glitch we
  inherited from dashboard v1 in which **tool tips were cropped** at the chart
  boundaries.

There are numerous other UI improvements and additional components that are being worked on.

WIP/Review
==========

The following features are still marked as "work in progress" or under code
review, but close to getting merged:

* `PR#20962 <https://github.com/ceph/ceph/pull/20962>`_ will add the frontend
  support to the **asynchronous task manager**, providing  a visual
  representation of all executing and most recently finished asynchronous tasks.

  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-taskmanager-ui.png

* `PR#20869 <https://github.com/ceph/ceph/pull/20869>`_ (frontend part) and
  `PR#21258 <https://github.com/ceph/ceph/pull/21258>`_ (backend code) will add
  **management support for the Ceph Object Gateway** (RGW), e.g. managing
  users, keys and buckets. 

  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-rgw-users.png
  .. image:: /galleries/ceph-dashboard-v2-screenshots-2018-04-06/dashboard-v2-rgw-buckets.png

* `PR#20920 <https://github.com/ceph/ceph/pull/20920>`_ adds **erasure code
  profile management** to the backend (which is needed for the Pool management) 

* `PR#21066 <https://github.com/ceph/ceph/pull/21066>`_ improves the **exception
  handling** in the backend.

Some other features that are currently being worked on, but no pull requests have been submitted yet:

* **Block device (RBD) management**: adding/removing/copying/snapshotting
* **Ceph Pool management**: create/delete pools and EC profiles
* A **configuration settings editor**
* **Localization** of the WebUI (we're currently evaluating POEditor vs.
  Transifex to facilitate translation support by the community)