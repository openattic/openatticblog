.. title: How to Blog on openATTIC.org?
.. slug: how-to-blog-on-openatticorg
.. date: 2015-09-18 09:45:18 UTC+02:00
.. tags: howto  
.. category: 
.. link: 
.. description:  
.. type: text
.. author: Kai Wagner

**This blog post is for openATTIC developers only. The mercurial repository and the internal IRC is not reachable outsite the intranet.**

Our new blog is released, but no one of our team (except Lenz and me) can write blog post, because they don´t know how to do it :-).

First of all you should install nikola and virtualenv, because that makes things so much easier. 

My Distribution - Debian 8.0 - Jessie

.. TEASER_END

The best way to do it is to use pip in a virtualenv.

So before you can start with the configuration and installation, you need some extra packages::

	apt-get install python-dev libxml2-dev libxslt1-dev zlib1g-dev python-virtualenv python-pip mercurial libjpeg-dev libfreetype6-dev

Now we can create the virtualenv, install nikola and clone our openattic repository::

	$ virtualenv nikola
	
	$ cd nikola
	
	$ source bin/activate
	
	$ pip install --upgrade "Nikola[extras]"
	...snip...
	Successfully installed Nikola

	$ hg clone https://hg.it-novum.com/bcc/openatticblog

	$ cd openatticblog

	$ nikola build

	$ nikola serve 
	INFO: serve: Serving HTTP on 0.0.0.0 port 8000

Now you can use your browser to connect to http://ip-of-your-machine:8000 and take a look at the blog. 

If you want to write new posts, you have to insert the following command::

	$ nikola new_post -a "Author Name" -t "Titel of your post"

Nikola creates a new post within the posts folder. You can now start writing inside that newly created file. `Quick reStructuredText documentation <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_

At any time you can use **'nikola build'** to refresh the blog output and **'nikola serve'** to check the current blog post layout.

When you're done, you have to commit and push your file to the mercurial repository. After 15 minutes the jenkins server will recognize the change
inside the repo and start building the new blog. If you cant wait and you are in a hurry, you can use our internal IRC bot and trigger the build **'#build openatticblog'**

