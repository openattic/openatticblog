.. title: Megacli LSI Script
.. slug: megacli-lsi-script
.. date: 2016-06-06 08:10:14 UTC+02:00
.. tags: megacli, raid, controller, disks, status 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

MegaCLI is the command line interface (CLI) to communicate with the LSI raid controllers. The syntax of that binary is really confusing and most of the time you have to google and do research for the commands over and over again. Today i found a little script from the guys from `calomel.org <https://calomel.org>`_. The script is really helpful. So, if you need a script for megacli, pleaes take a look at that articel - `megacli_lsi_commands <https://calomel.org/megacli_lsi_commands.html>`_. 
