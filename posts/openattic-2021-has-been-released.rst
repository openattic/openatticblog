.. title: openATTIC 2.0.21 has been released
.. slug: openattic-2021-has-been-released
.. date: 2018-02-28 15:58:43 UTC+01:00
.. tags: openattic, bugfix, release 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

We are very happy to announce the release of openATTIC version 2.0.21. This is mainly a bugfix release.

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

.. TEASER_END

Changelog for version 2.0.21
----------------------------

Added
~~~~~
* Packaging: make_dist.py: Added `--sign` option to sign tarballs with GPG
  (:issue:`OP-2124`)
* Backend: Corrected `openattic-systemd.service` file. (:issue:`OP-2570`)

Changed
~~~~~~~
* Backend: Corrected num_bytes unit in check_cephpool Nagios plugin (:issue:`OP-1532`)
* Backend/WebUI: The monitoring of RBDs without 'fast-diff' feature has been
  disabled due to performance reasons (:issue:`OP-2141`, :issue:`OP-2223`)
* Packaging: back-ported a number of improvements to `make_dist.py`, to keep
  it in sync with the master branch and to simplify the packaging process. (:issue:`OP-2056`)

Fixed
~~~~~
* WebUI: Redirect to dashboard when an authenticated user navigates to login page.
  Thanks to Raúl Vega for the contribution. (:issue:`OP-2029`)
* ``oaconfig install`` does no longer fail if a Ceph cluster is inaccessible
  while updating the Nagios configs (:issue:`OP-2291`)
* Backend: Fix an issue in a multinode setup caused by an newer DRF
  version in Ubuntu 16.04.(:issue:`OP-2138`)
* Backend: Add formulas to calculate the usage of a Ceph pool correctly (for
  Ceph Luminous and Jewel) (:issue:`OP-2128`)
* Backend: Fixed error in `get_rbd_performance_data` task by catching an
  exception (:issue:`OP-2057`)
* Packaging: Enable byte-compiling the Python files during RPM packaging
  (:issue:`OP-2167`)
* Packaging: Fix internal commit functionality of `make_dist.py` (:issue:`OP-2227`)
