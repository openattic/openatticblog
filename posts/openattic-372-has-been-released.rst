.. title: openATTIC 3.7.2 has been released
.. slug: openattic-372-has-been-released
.. date: 2019-03-22 15:45:30 UTC+01:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.7.2 release
.. type: text
.. author: Laura Paduano

We're happy to announce version 3.7.2 of openATTIC!

Version 3.7.2 is the second release in 2019 and although it does not
contain a **lot** of changes it includes a critical bugfix when using or
updating to Ceph Luminous version 12.2.11-566-g896835fd74.
When using the previously mentioned Ceph version it was no longer
possible to create a replicated pool within openATTIC - trying to do
so resulted in an error.

Also we fixed an issue in the frontend related to long running tasks showing
a huge time difference for calculated runtimes even though they only took a
few seconds to complete.

Fortunately we were able to fix the mentioned issues and are therefore happy
to release a new openATTIC version including these fixes and making sure
that when updating to Ceph version 12.2.11-x openATTIC works as expected.


.. TEASER_END

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.7.2
---------------------------

Fixed
~~~~~
* WebUI: Task time calculations in different time zones (:issue:`OP-3178`)
* WebUI/QA: Input fields are cleared manual (:issue:`OP-3187`)
* Backend: Gatling test "error parsing integer value ..." (:issue:`OP-3188`)
