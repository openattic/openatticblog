.. title: Planned service outage this weekend
.. slug: planned-service-outage-this-weekend
.. date: 2017-10-13 13:16:45 UTC+02:00
.. tags: announcement, community, collaboration
.. category: 
.. link: 
.. description: Announcing a community service disruption
.. type: text
.. author: Lenz Grimmer

Due to some `planned maintenance work
<https://news.opensuse.org/2017/10/04/planned-downtime-to-affect-opensuse-services/>`_
in SUSE's data center, a number of services will not be available from Friday,
Oct. 13th, 16:00 UTC until Monday, Oct. 16th, 12:00 UTC.

In particular, the following services related to the openATTIC project will not
be available:

* `JIRA <https://tracker.openattic.org/>`_
* `Confluence <https://wiki.openattic.org/>`_
* `Downloads <http://download.openattic.org>`_
* Downloads from SUSE's `Build Service <https://software.opensuse.org/package/openattic>`_

The openATTIC web site and blog as well as the `documentation
<http://docs.openattic.org>`_ will not be affected by this.
