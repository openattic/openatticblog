.. title: Reduce KVM disk size with dd and sparsify
.. slug: reduce-kvm-disk-size-with-dd-and-sparsify
.. date: 2016-11-01 09:45:38 UTC+01:00
.. tags: kvm, disk, reduce, dd, sparsify, virsh, virt
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

You can convert a raw or qcow2 non-sparse image to a sparse image with dd and sparsify. Or you can reduce the size of an existing image again.

Install the libguestfs-tools on your system

	apt-get install libguestfs-tools

Now copy your existing image to a new one with dd

	dd if=existing_imagefile.raw of=new_imagefile.raw conv=sparse

Afterwards use virt-sparsify to reduce your disk size again (in this example I sparsed and converted the image in just one step)

	virt-sparsify new_imagefile.raw --convert qcow2 new_imagefile.qcow2

In my case I converted a block device with 65GB with dd sparse to 40GB raw image and afterwards I used virt-sparsify to reduce the size down to 6.8GB.

