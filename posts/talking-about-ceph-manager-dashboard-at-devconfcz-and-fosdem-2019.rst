.. title: Talking about Ceph Manager Dashboard at DevConf.CZ and FOSDEM 2019
.. slug: talking-about-ceph-manager-dashboard-at-devconfcz-and-fosdem-2019
.. date: 2019-01-15 14:37:38 UTC+01:00
.. tags:  ceph, community, conference, dashboard, development, event, fosdem, devconf.cz, news, mgr, presentation, suse
.. category: 
.. link: 
.. description: Highlighting some upcoming presentations about the Ceph Manager Dashboard
.. type: text
.. author: Lenz Grimmer

Happy New Year to all of you! It's been a while that I posted on this blog; our
team has been through an exciting year and it's time to talk about our
achievements so far!

We've made a lot of progress in adding new features and missing functionality to
the `Ceph Manager Dashboard <http://docs.ceph.com/docs/master/mgr/dashboard/>`_.
The upcoming Ceph 14.0 "Nautilus" release is going to be a major milestone for
us.

.. thumbnail:: /images/ceph-dashboard-landingpage-2019-01-15.png

If you're curious to learn more about our work, I'm happy to share that I'll be
talking about the dashboard at two upcoming Open Source conferences in Europe:

* On Saturday, 26th of January, I will speak about
  `Ceph Management and Monitoring with the Dashboard
  <https://devconfcz2019.sched.com/event/1417cc7f9e8aefb538f69c0c62545175/>`_ at
  the `DevConf.CZ 2019 <https://devconf.info/cz>`_ in Brno, Czech Republic.

* On Sunday, 3rd of February, I will give an introduction to
  `Managing and Monitoring Ceph with the Ceph Manager Dashboard
  <https://fosdem.org/2019/schedule/event/ceph_manager_dashboard/>`_ in the
  `Software Defined Storage Developer Room
  <https://fosdem.org/2019/schedule/track/software_defined_storage/>`_ at
  `FOSDEM 2019 <https://fosdem.org/2019/>`_ in Brussels, Belgium.