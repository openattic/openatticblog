.. title: The initial Ceph Dashboard v2 pull request has been merged!
.. slug: the-initial-ceph-dashboard-v2-pull-request-has-been-merged
.. date: 2018-03-13 10:02:16 UTC+01:00
.. tags: ceph, community, development, mgr, dashboard
.. category: 
.. link: 
.. description: Announcing the merge of the initial Dashboard v2 pull request
.. type: text
.. author: Lenz Grimmer

It actually happened exactly one week ago while I was on vacation: it's our
great pleasure and honor to announce that we have reached our first milestone -
the :doc:`initial Ceph Dashboard v2 pull request
<the-ceph-dashboard-v2-pull-request-is-ready-for-review>` has now been `merged
<https://www.spinics.net/lists/ceph-devel/msg40361.html>`_ into the upstream
Ceph master git branch, so it will become part of the upcoming Ceph "Mimic"
release!

.. youtube:: m5i3x4eR6k4

.. TEASER_END

I'd like to express my gratitude to the team at SUSE that tirelessly worked on
this project for the last 7-8 weeks and everyone else who contributed by
reviewing/commenting and giving guidance. In particular, we would like to thank
John Spray from the Ceph project for his help and support.

And we're far from done yet - we're already working on many follow-up
improvements and new features, including additional object gateway management
features (e.g. `list users/keys/buckets
<https://github.com/ceph/ceph/pull/20869>`_), `RBD management
<https://github.com/ceph/ceph/pull/20852>`_, SSL support, and a few other
enhancements.

Stay tuned for additional `pull requests
<https://github.com/ceph/ceph/pulls?q=is%3Aopen+is%3Apr+label%3Adashboard>`_
being submitted in the coming days and weeks.

What I find particularly noteworthy is the fact that we already received pull
requests and feedback from other contributors outside of the original team at
SUSE that started this project. We look forward to getting more of these!

If you would like to take a look at the new Dashboard UI you can either watch
the screen cast above, or set up your own local development environment. See the
file `HACKING.rst
<https://github.com/ceph/ceph/blob/master/src/pybind/mgr/dashboard_v2/HACKING.rst>`_
in the dashboard module's subdirectory for details.

Thanks again to everyone involved in making this happen! This is a major
milestone in our project's history.
