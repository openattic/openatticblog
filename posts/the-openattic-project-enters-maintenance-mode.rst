.. title: The openATTIC project enters maintenance mode
.. slug: the-openattic-project-enters-maintenance-mode
.. date: 2019-08-22 16:20:01 UTC+02:00
.. tags: announcement, ceph, collaboration, community, development, dashboard
.. category:
.. link:
.. description: Announcing the maintenance mode for openATTIC
.. type: text
.. author: Lenz Grimmer

If you have been following this blog closely, you may have already noticed that
the frequency of new openATTIC releases and `development activity
<https://www.openhub.net/p/openattic/commits/summary>`_ in general has been
slowing down over the course of the last 1.5 years.

We've been doing several `bug fix releases <link://tag/release/>`_ of
openATTIC 3.7 (and will continue to do so), but have not released a new major
version with new features for quite some time. We also have not added support
for Ceph "Mimic" or "Nautilus" (the current supported version of Ceph in
openATTIC is "Luminous").

Instead, we have spent most of our energy in `enhancing and improving
<link://tag/dashboard/>`_ the upstream `Ceph Dashboard
<https://docs.ceph.com/docs/master/mgr/dashboard/>`_ to become a full
replacement for openATTIC. And we're happy to announce that we've reached
feature parity (and much more) with openATTIC with the current Ceph Nautilus
release!

.. thumbnail:: /images/ceph-dashboard-landingpage-2019-07-01.png

Ceph Dashboard has been developed nearly from scratch, and we have been able to
address a number of shortcomings that we identified in the openATTIC
architecture over the course of its life cycle. There's great value in being
able to start over again and to rethink some previous design decisions; we're
very grateful to the Ceph community for giving us this opportunity.

Moving our focus upstream also allowed us to gather a much larger developer
community around our efforts, which helped tremendously in adding new features
and improving the existing functionality in a very short period of time. I am
very proud of what we have achieved over the course of the past 20 months and we
are very thankful for all the contributions and feedback that we've received so
far!

Working with the upstream Ceph project directly also has the additional benefit
of being part of the software distribution "out of the box" - there's no need to
install any additional software! This way, we're able to reach a much wider
audience of users.

If you have been using openATTIC before, many parts of the Dashboard will likely
look familiar to you. We hope that switching to this new web-based management
tool will be seen as an improvement and step forward from a user perspective.

If you are missing anything in the Dashboard that openATTIC used to provide,
please `get in touch with us <https://ceph.com/irc/>`_ and let us know! We're
very keen on getting feedback as well as contributions.
