.. title: Use your own javascript templates with nikola
.. slug: use-your-own-javascript-templates-with-nikola
.. date: 2015-10-02 12:45:23 UTC+02:00
.. tags: howto, javascript 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

You can use your own javascript templates with nikola.

Just edit the **BODY_END** parameter in the conf.py file.

	BODY_END = "<script src=\"/assets/js/showhide.js\"</script>"
