.. title: Video and Slides of the openATTIC Overview Talk at FrOSCon 2016
.. slug: video-and-slides-of-the-openattic-overview-talk-at-froscon-2016
.. date: 2016-08-29 17:10:35 UTC+02:00
.. tags: community, event, froscon, conference, community, collaboration, opensource, presentation
.. category:
.. link:
.. description: Announcing the availability of the FrOSCON Slides and Video
.. type: text
.. author: Lenz Grimmer

About a week ago, the openATTIC team attended the annual Free and Open Source
Conference (`FrOSCon <https://www.froscon.de/en/home/>`_) in St. Augustin,
Germany.

We had a booth in the exhibition area and we also gave an `Overview talk about
openATTIC 2.0 <https://programm.froscon.de/2016/events/1755.html>`_ (in
German), highlighting the latest changes and features as well as an outlook
into future development plans.

The slides of this talk have now been uploaded to `SlideShare
<http://www.slideshare.net/LenzGr/ceph-and-storage-management-with-openattic-froscon-20160821>`_,
and a video recording of the presentation is available on `YouTube
<https://youtu.be/rqT9QufIm1U>`_ and `C3TV
<https://media.ccc.de/v/froscon2016-1755-open_source_storage_management_mit_openattic>`_
- enjoy!

Video (YouTube):

.. youtube:: rqT9QufIm1U

Slides:

.. media:: http://www.slideshare.net/LenzGr/ceph-and-storage-management-with-openattic-froscon-20160821
