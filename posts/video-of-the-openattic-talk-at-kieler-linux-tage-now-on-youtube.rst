.. title: Video of the openATTIC talk at Kieler Linux Tage now on YouTube
.. slug: video-of-the-openattic-talk-at-kieler-linux-tage-now-on-youtube
.. date: 2015-11-10 13:03:01 UTC+01:00
.. tags: presentation, video, youtube, slideshare, conference, event
.. category: 
.. link: 
.. description: The Video of the openATTIC talk at Kieler Linux Tage has now
been published on YouTube.
.. type: text
.. author: Lenz Grimmer

Some time ago, I attended the `Kieler Linux Tage
<http://www.kieler-linuxtage.de>`_, to talk about managing storage on Linux
using openATTIC.

The post processing took a while, but the video recording of my presentation
titled "Flexibles Storage Management unter Linux mit openATTIC" (in German) is
now available on YouTube (in 4K resolution!):

.. youtube:: S2ouxENWRf8

I hope you enjoy it! A big Thank You to `LuiKast
<https://www.youtube.com/c/LuiKast>`_ for the professional recording and
post-processing.

The slide deck is also available on `SlideShare
<http://www.slideshare.net/LenzGr/flexibles-storage-management-unter-linux-mit-openattic>`_:

.. media:: http://www.slideshare.net/LenzGr/flexibles-storage-management-unter-linux-mit-openattic
